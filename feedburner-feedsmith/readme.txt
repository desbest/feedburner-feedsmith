=== Feedburner Feedsmith ===
Contributors: desbest
Tags: feedburner, rss, feeds
Donate link: http://paypal.me/tynamiteuk
Requires at least: 3
Tested up to: 5.2
Requires PHP: 5.2.4
Stable tag: trunk
License: MIT
License URI: https://en.wikipedia.org/wiki/MIT_License

Redirect your RSS feeds to a FeedBurner feed with this plugin.

== Description ==
Redirect your RSS feeds to a FeedBurner feed with this plugin.
Contributors: Steve Smith, FeedBurner, desbest
Tags: rss, feeds, feedburner
Requires at least: 1.5
Tested up to: 5.2
Stable tag: 2.9.2

== New maintainer ==
This plugin is now maintained by desbest.
http://desbest.com/projects/feedburner-feedsmith

== Installation ==

To do a new installation of the plugin, please follow these steps

1. Download the zipped plugin file to your local machine.
2. Unzip the file.
3. Upload the `feedburner-feedsmith` folder to the `/wp-content/plugins/` directory.
4. Activate the plugin through the 'Plugins' menu in WordPress.
5. Follow the instructions in the FeedBurner section of the settings.

If you have already installed the plugin

1. De-activate the plugin.
2. Download the latest files.
2. Follow the new installation steps.